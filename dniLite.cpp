//---------------------------------------------------------------------------
//
// Name:        dniLite.cpp
// Author:      Firedancer Software
// Created:     22/06/2013 1:48:36 PM
// Description:
//
//---------------------------------------------------------------------------

#include "dniLite.h"
#include "Objects/MingW/dniLite_private.h"
#include <wx/tokenzr.h>
#include <wx/arrstr.h>

// In case the machine this is compiled on does not have the most recent platform SDK
// with these values defined, define them here
#ifndef SM_TABLETPC
	#define SM_TABLETPC     86
#endif

#ifndef SM_MEDIACENTER
	#define SM_MEDIACENTER  87
#endif

#define CountOf(x) sizeof(x)/sizeof(*x)


IMPLEMENT_APP(dniLite)


// Constants that represent registry key names and value names
// to use for detection
const TCHAR *g_szNetfx10RegKeyName = _T("Software\\Microsoft\\.NETFramework\\Policy\\v1.0");
const TCHAR *g_szNetfx10RegKeyValue = _T("3705");
const TCHAR *g_szNetfx10SPxMSIRegKeyName = _T("Software\\Microsoft\\Active Setup\\Installed Components\\{78705f0d-e8db-4b2d-8193-982bdda15ecd}");
const TCHAR *g_szNetfx10SPxOCMRegKeyName = _T("Software\\Microsoft\\Active Setup\\Installed Components\\{FDC11A6F-17D1-48f9-9EA3-9051954BAA24}");
const TCHAR *g_szNetfx11RegKeyName = _T("Software\\Microsoft\\NET Framework Setup\\NDP\\v1.1.4322");
const TCHAR *g_szNetfx20RegKeyName = _T("Software\\Microsoft\\NET Framework Setup\\NDP\\v2.0.50727");
const TCHAR *g_szNetfx30RegKeyName = _T("Software\\Microsoft\\NET Framework Setup\\NDP\\v3.0\\Setup");
const TCHAR *g_szNetfx30SpRegKeyName = _T("Software\\Microsoft\\NET Framework Setup\\NDP\\v3.0");
const TCHAR *g_szNetfx30RegValueName = _T("InstallSuccess");
const TCHAR *g_szNetfx35RegKeyName = _T("Software\\Microsoft\\NET Framework Setup\\NDP\\v3.5");
const TCHAR *g_szNetfx40ClientRegKeyName = _T("Software\\Microsoft\\NET Framework Setup\\NDP\\v4\\Client");
const TCHAR *g_szNetfx40FullRegKeyName = _T("Software\\Microsoft\\NET Framework Setup\\NDP\\v4\\Full");
const TCHAR *g_szNetfx40SPxRegValueName = _T("Servicing");
const TCHAR *g_szNetfx45ClientRegKeyName = _T("Software\\Microsoft\\NET Framework Setup\\NDP\\v4\\Client");
const TCHAR *g_szNetfx45FullRegKeyName = _T("Software\\Microsoft\\NET Framework Setup\\NDP\\v4\\Full");
const TCHAR *g_szNetfx45SPxRegValueName = _T("Servicing");
const TCHAR *g_szNetfx45RegValueName = _T("Release");
const TCHAR *g_szNetfxStandardRegValueName = _T("Install");
const TCHAR *g_szNetfxStandardSPxRegValueName = _T("SP");
const TCHAR *g_szNetfxStandardVersionRegValueName = _T("Version");

// Version information for final release of .NET Framework 3.0
const int g_iNetfx30VersionMajor = 3;
const int g_iNetfx30VersionMinor = 0;
const int g_iNetfx30VersionBuild = 4506;
const int g_iNetfx30VersionRevision = 26;

// Version information for final release of .NET Framework 3.5
const int g_iNetfx35VersionMajor = 3;
const int g_iNetfx35VersionMinor = 5;
const int g_iNetfx35VersionBuild = 21022;
const int g_iNetfx35VersionRevision = 8;

// Version information for final release of .NET Framework 4
const int g_iNetfx40VersionMajor = 4;
const int g_iNetfx40VersionMinor = 0;
const int g_iNetfx40VersionBuild = 30319;
const int g_iNetfx40VersionRevision = 0;

// Version information for final release of .NET Framework 4.5
const int g_iNetfx45VersionMajor = 4;
const int g_iNetfx45VersionMinor = 5;
const int g_iNetfx45VersionBuild = 50709;
const int g_iNetfx45VersionRevision = 0;
//	Actual Version currently reported in registry features only Major.Minor.Build
//	Revision should be set to zero (at least for now) to produce a valid match
//const int g_iNetfx45VersionRevision = 17929;

// Constants for known .NET Framework versions used with the GetRequestedRuntimeInfo API
const TCHAR *g_szNetfx10VersionString = _T("v1.0.3705");
const TCHAR *g_szNetfx11VersionString = _T("v1.1.4322");
const TCHAR *g_szNetfx20VersionString = _T("v2.0.50727");
const TCHAR *g_szNetfx40VersionString = _T("v4.0.30319");
const TCHAR *g_szNetfx45VersionString = _T("v4.5.50709");



bool dniLite::OnInit()
{
	wxString cmdLogo = wxT("dniLite ");
	cmdLogo << VER_MAJOR << wxT(".") << VER_MINOR << wxT(".") << VER_RELEASE << wxT(" by Firedancer Software");
	parser.SetLogo(cmdLogo);
	parser.SetDesc(g_cmdLineDesc);
	wxString usageText = wxT("\n");
	usageText << wxT("By default dniLite outputs 1 via stdout if the framework");
	usageText << wxT(" version (and service pack) is found or 0 if not found.");
	usageText << wxT(" These values can be changed using the smt and smf options.\n\n");
	usageText << wxT("The smt, smf, dmt and dmf options can access the framework version");
	usageText << wxT(" and service pack passed to the fv and sp options using #fv# and #sp#.\n");
	parser.AddUsageText(usageText);
	parser.SetCmdLine(argc, argv);
	parser.SetSwitchChars(wxT("-"));
	int iParseVal = parser.Parse();

	if(iParseVal != 0)
	{
		return 0;
	}

	stdoutMessageFound = wxT("1");
	stdoutMessageNotFound = wxT("0");

	bCheckVersion = parser.Found(wxT("fv"), &version);
	bCheckGreaterThan = parser.Found(wxT("gt"));
	bCheckLessThan = parser.Found(wxT("lt"));
	bCheckServicePack = parser.Found(wxT("sp"), &lServicePack);
	bExecuteIfFound = parser.Found(wxT("et"), &executeIfFoundPath);
	bExecuteIfNotFound = parser.Found(wxT("ef"), &executeIfNotFoundPath);

	parser.Found(wxT("smt"), &stdoutMessageFound);
	parser.Found(wxT("smf"), &stdoutMessageNotFound);

	bDialogMessageFound = parser.Found(wxT("dmt"), &dialogMessageFound);
	bDialogMessageNotFound = parser.Found(wxT("dmf"), &dialogMessageNotFound);


	if(bCheckVersion)
	{
		bool bPackageInstalled = false;


		wxArrayString availableVersions;
		availableVersions.Alloc(9);
		availableVersions.Add(wxT("1.0"));
		availableVersions.Add(wxT("1.1"));
		availableVersions.Add(wxT("2.0"));
		availableVersions.Add(wxT("3.0"));
		availableVersions.Add(wxT("3.5"));
		availableVersions.Add(wxT("4.0")); // Pseudo-version that will check for the existence of 4.0full or 4.0client
		availableVersions.Add(wxT("4.0client"));
		availableVersions.Add(wxT("4.0full"));
		availableVersions.Add(wxT("4.5"));

		wxArrayString checkVersions;

		if(availableVersions.Index(version) == wxNOT_FOUND)
		{
			parser.Usage();
			return 0;
		}

		int i;
		int iMax;

		if(bCheckGreaterThan || bCheckLessThan)
		{
			// Do not check service pack if range is specified
			bCheckServicePack = false;

			if(bCheckGreaterThan)
			{
				for(i = availableVersions.Index(version) + 1, iMax = availableVersions.GetCount(); i < iMax; i++)
				{
					checkVersions.Add(availableVersions[i]);
				}
			}
			if(bCheckLessThan)
			{
				for(i = availableVersions.Index(version) - 1; i >= 0; i--)
				{
					checkVersions.Add(availableVersions[i]);
				}
			}
		}
		else
		{
			checkVersions.Add(version);
		}
		checkVersions.Shrink();

		if(!checkVersions.IsEmpty())
		{
			for(i = 0, iMax = checkVersions.GetCount(); i < iMax; i++)
			{
				if(checkVersions[i] == "1.0")
				{
					if(IsNetfx10Installed())// && CheckNetfxVersionUsingMscoree(g_szNetfx10VersionString))
					{
						if(bCheckServicePack && GetNetfx10SPLevel() != lServicePack)
						{
							bPackageInstalled = false;
							continue;
						}

						bPackageInstalled = true;
						break;
					}
				}
				else if(checkVersions[i] == "1.1")
				{
					if(IsNetfx11Installed())// && CheckNetfxVersionUsingMscoree(g_szNetfx11VersionString))
					{
						if(bCheckServicePack && GetNetfxSPLevel(g_szNetfx11RegKeyName, g_szNetfxStandardSPxRegValueName) != lServicePack)
						{
							bPackageInstalled = false;
							continue;
						}

						bPackageInstalled = true;
						break;
					}
				}
				else if(checkVersions[i] == "2.0")
				{
					if(IsNetfx20Installed())// && CheckNetfxVersionUsingMscoree(g_szNetfx20VersionString))
					{
						if(bCheckServicePack && GetNetfxSPLevel(g_szNetfx20RegKeyName, g_szNetfxStandardSPxRegValueName) != lServicePack)
						{
							bPackageInstalled = false;
							continue;
						}

						bPackageInstalled = true;
						break;
					}
				}
				else if(checkVersions[i] == "3.0")
				{
					// Need to validate that both 2.0 and 3.0 are installed.
					if(IsNetfx20Installed() && IsNetfx30Installed())// && CheckNetfxVersionUsingMscoree(g_szNetfx20VersionString))
					{
						if(bCheckServicePack && GetNetfxSPLevel(g_szNetfx30SpRegKeyName, g_szNetfxStandardSPxRegValueName) != lServicePack)
						{
							bPackageInstalled = false;
							continue;
						}

						bPackageInstalled = true;
						break;
					}
				}
				else if(checkVersions[i] == "3.5")
				{
					// Need to validate that 2.0, 3.0 and 3.5 are installed.
					if(IsNetfx20Installed() && IsNetfx30Installed() && IsNetfx35Installed())// && CheckNetfxVersionUsingMscoree(g_szNetfx20VersionString))
					{
						if(bCheckServicePack && GetNetfxSPLevel(g_szNetfx35RegKeyName, g_szNetfxStandardSPxRegValueName) != lServicePack)
						{
							bPackageInstalled = false;
							continue;
						}

						bPackageInstalled = true;
						break;
					}
				}
				else if(checkVersions[i] == "4.0")
				{
					if(IsNetfx40ClientInstalled())// && CheckNetfxVersionUsingMscoree(g_szNetfx40VersionString))
					{
						if(bCheckServicePack && GetNetfxSPLevel(g_szNetfx40ClientRegKeyName, g_szNetfx40SPxRegValueName) != lServicePack)
						{
							bPackageInstalled = false;
							continue;
						}

						bPackageInstalled = true;
						break;
					}
					else if(IsNetfx40FullInstalled())// && CheckNetfxVersionUsingMscoree(g_szNetfx40VersionString))
					{
						if(bCheckServicePack && GetNetfxSPLevel(g_szNetfx40FullRegKeyName, g_szNetfx40SPxRegValueName) != lServicePack)
						{
							bPackageInstalled = false;
							continue;
						}

						bPackageInstalled = true;
						break;
					}
				}
				else if(checkVersions[i] == "4.0client")
				{
					if(IsNetfx40ClientInstalled())// && CheckNetfxVersionUsingMscoree(g_szNetfx40VersionString))
					{
						if(bCheckServicePack && GetNetfxSPLevel(g_szNetfx40ClientRegKeyName, g_szNetfx40SPxRegValueName) != lServicePack)
						{
							bPackageInstalled = false;
							continue;
						}

						bPackageInstalled = true;
						break;
					}
				}
				else if(checkVersions[i] == "4.0full")
				{
					if(IsNetfx40FullInstalled())// && CheckNetfxVersionUsingMscoree(g_szNetfx40VersionString))
					{
						if(bCheckServicePack && GetNetfxSPLevel(g_szNetfx40FullRegKeyName, g_szNetfx40SPxRegValueName) != lServicePack)
						{
							bPackageInstalled = false;
							continue;
						}

						bPackageInstalled = true;
						break;
					}
				}
				else if(checkVersions[i] == "4.5")
				{
					// Need to validate that 4.0 client or 4.0 full are also installed.
					if(IsNetfx40FullInstalled() && IsNetfx45Installed())// && CheckNetfxVersionUsingMscoree(g_szNetfx40VersionString))
					{
						if(bCheckServicePack && GetNetfxSPLevel(g_szNetfx45FullRegKeyName, g_szNetfx45SPxRegValueName) != lServicePack)
						{
							bPackageInstalled = false;
							continue;
						}

						bPackageInstalled = true;
						break;
					}
					else if(IsNetfx40ClientInstalled() && IsNetfx45Installed())// && CheckNetfxVersionUsingMscoree(g_szNetfx40VersionString))
					{
						if(bCheckServicePack && GetNetfxSPLevel(g_szNetfx45ClientRegKeyName, g_szNetfx45SPxRegValueName) != lServicePack)
						{
							bPackageInstalled = false;
							continue;
						}

						bPackageInstalled = true;
						break;
					}

//					bPackageInstalled = false;
//					continue;
				}
				else
				{
					parser.Usage();
					return 0;
				}
			}
		}

		wxString servicePack = wxT("");

		if(bCheckServicePack)
		{
			servicePack << lServicePack;
		}

		if(bPackageInstalled)
		{
			stdoutMessageFound.Replace(wxT("#fv#"), version);
			stdoutMessageFound.Replace(wxT("#sp#"), servicePack);

			std::cout << stdoutMessageFound << std::endl;
			if(bDialogMessageFound)
			{
				dialogMessageFound.Replace(wxT("#fv#"), version);
				dialogMessageFound.Replace(wxT("#sp#"), servicePack);

				MessageBox(NULL, dialogMessageFound, wxT("dniLite"), MB_OK | MB_ICONWARNING);
			}

			if(bExecuteIfFound)
			{
				wxExecute(executeIfFoundPath);
			}
		}
		else
		{

			stdoutMessageNotFound.Replace(wxT("#fv#"), version);
			stdoutMessageNotFound.Replace(wxT("#sp#"), servicePack);

			std::cout << stdoutMessageNotFound << std::endl;

			if(bDialogMessageNotFound)
			{
				dialogMessageNotFound.Replace(wxT("#fv#"), version);
				dialogMessageNotFound.Replace(wxT("#sp#"), servicePack);

				MessageBox(NULL, dialogMessageNotFound, wxT("dniLite"), MB_OK | MB_ICONERROR);
			}

			if(bExecuteIfNotFound)
			{
				wxExecute(executeIfNotFoundPath);
			}
		}
		return 0;
	}
	else
	{
		parser.Usage();
		return 0;
	}
	return true;
}

int dniLite::OnExit()
{
	return 0;
}


/******************************************************************
Function Name:  CheckNetfxVersionUsingMscoree
Description:    Uses the logic described in the sample code at
                http://msdn2.microsoft.com/library/ydh6b3yb.aspx
                to load mscoree.dll and call its APIs to determine
                whether or not a specific version of the .NET
                Framework is installed on the system
Inputs:         pszNetfxVersionToCheck - version to look for
Results:        true if the requested version is installed
                false otherwise
******************************************************************/
/*bool dotNetInspectorFrm::CheckNetfxVersionUsingMscoree(const TCHAR *pszNetfxVersionToCheck)
{
	bool bFoundRequestedNetfxVersion = false;
	HRESULT hr = S_OK;

	// Check input parameter
	if (NULL == pszNetfxVersionToCheck)
		return false;

	HMODULE hmodMscoree = LoadLibraryEx(_T("mscoree.dll"), NULL, 0);
	if (NULL != hmodMscoree)
	{
		typedef HRESULT (STDAPICALLTYPE *GETCORVERSION)(LPWSTR szBuffer, DWORD cchBuffer, DWORD* dwLength);
		GETCORVERSION pfnGETCORVERSION = (GETCORVERSION)GetProcAddress(hmodMscoree, "GetCORVersion");

		// Some OSs shipped with a placeholder copy of mscoree.dll. The existence of mscoree.dll
		// therefore does NOT mean that a version of the .NET Framework is installed.
		// If this copy of mscoree.dll does not have an exported function named GetCORVersion
		// then we know it is a placeholder DLL.
		if (NULL == pfnGETCORVERSION)
			goto Finish;

		typedef HRESULT (STDAPICALLTYPE *CORBINDTORUNTIME)(LPCWSTR pwszVersion, LPCWSTR pwszBuildFlavor, REFCLSID rclsid, REFIID riid, LPVOID FAR *ppv);
		CORBINDTORUNTIME pfnCORBINDTORUNTIME = (CORBINDTORUNTIME)GetProcAddress(hmodMscoree, "CorBindToRuntime");

		typedef HRESULT (STDAPICALLTYPE *GETREQUESTEDRUNTIMEINFO)(LPCWSTR pExe, LPCWSTR pwszVersion, LPCWSTR pConfigurationFile, DWORD startupFlags, DWORD runtimeInfoFlags, LPWSTR pDirectory, DWORD dwDirectory, DWORD *dwDirectoryLength, LPWSTR pVersion, DWORD cchBuffer, DWORD* dwlength);
		GETREQUESTEDRUNTIMEINFO pfnGETREQUESTEDRUNTIMEINFO = (GETREQUESTEDRUNTIMEINFO)GetProcAddress(hmodMscoree, "GetRequestedRuntimeInfo");

		if (NULL != pfnCORBINDTORUNTIME)
		{
			WCHAR szRetrievedVersion[50];
			DWORD dwLength = CountOf(szRetrievedVersion);

			if (NULL == pfnGETREQUESTEDRUNTIMEINFO)
			{
				// Having CorBindToRuntimeHost but not having GetRequestedRuntimeInfo means that
				// this machine contains no higher than .NET Framework 1.0, but the only way to
				// 100% guarantee that the .NET Framework 1.0 is installed is to call a function
				// to exercise its functionality
				if (0 == _tcscmp(pszNetfxVersionToCheck, g_szNetfx10VersionString))
				{
					hr = pfnGETCORVERSION(szRetrievedVersion, dwLength, &dwLength);

					if (SUCCEEDED(hr))
					{
						if (0 == _tcscmp(szRetrievedVersion, g_szNetfx10VersionString))
							bFoundRequestedNetfxVersion = true;
					}

					goto Finish;
				}
			}

			// Set error mode to prevent the .NET Framework from displaying
			// unfriendly error dialogs
			UINT uOldErrorMode = SetErrorMode(SEM_FAILCRITICALERRORS);

			TCHAR szDirectory[MAX_PATH];
			DWORD dwDirectoryLength = 0;
//			DWORD dwRuntimeInfoFlags = RUNTIME_INFO_DONT_RETURN_DIRECTORY | GetProcessorArchitectureFlag();
			DWORD dwRuntimeInfoFlags = GetProcessorArchitectureFlag();

			// Check for the requested .NET Framework version
			hr = pfnGETREQUESTEDRUNTIMEINFO(NULL, pszNetfxVersionToCheck, NULL, STARTUP_LOADER_OPTIMIZATION_MULTI_DOMAIN_HOST, NULL, szDirectory, CountOf(szDirectory), &dwDirectoryLength, szRetrievedVersion, CountOf(szRetrievedVersion), &dwLength);

			if (SUCCEEDED(hr))
				bFoundRequestedNetfxVersion = true;

			// Restore the previous error mode
			SetErrorMode(uOldErrorMode);
		}
	}

Finish:
	if (hmodMscoree)
	{
		FreeLibrary(hmodMscoree);
	}

	return bFoundRequestedNetfxVersion;
}

*/
/******************************************************************
Function Name:  GetNetfx10SPLevel
Description:    Uses the detection method recommended at
                http://blogs.msdn.com/astebner/archive/2004/09/14/229802.aspx
                to determine what service pack for the
                .NET Framework 1.0 is installed on the machine
Inputs:         NONE
Results:        integer representing SP level for .NET Framework 1.0
******************************************************************/
int dniLite::GetNetfx10SPLevel()
{
	TCHAR szRegValue[MAX_PATH];
	TCHAR *pszSPLevel = NULL;
	int iRetValue = -1;
	bool bRegistryRetVal = false;

	// Need to detect what OS we are running on so we know what
	// registry key to use to look up the SP level
	if (IsCurrentOSTabletMedCenter())
		bRegistryRetVal = RegistryGetValue(HKEY_LOCAL_MACHINE, g_szNetfx10SPxOCMRegKeyName, g_szNetfxStandardVersionRegValueName, NULL, (LPBYTE)szRegValue, MAX_PATH);
	else
		bRegistryRetVal = RegistryGetValue(HKEY_LOCAL_MACHINE, g_szNetfx10SPxMSIRegKeyName, g_szNetfxStandardVersionRegValueName, NULL, (LPBYTE)szRegValue, MAX_PATH);

	if (bRegistryRetVal)
	{
		// This registry value should be of the format
		// #,#,#####,# where the last # is the SP level
		// Try to parse off the last # here
		pszSPLevel = _tcsrchr(szRegValue, _T(','));
		if (NULL != pszSPLevel)
		{
			// Increment the pointer to skip the comma
			pszSPLevel++;

			// Convert the remaining value to an integer
			iRetValue = wxAtoi(pszSPLevel);
		}
	}

	return iRetValue;
}


/******************************************************************
Function Name:	GetNetfxSPLevel
Description:	Determine what service pack is installed for a
                version of the .NET Framework using registry
				based detection methods documented in the
				.NET Framework deployment guides.
Inputs:         pszNetfxRegKeyName - registry key name to use for detection
				pszNetfxRegValueName - registry value to use for detection
Results:        integer representing SP level for .NET Framework
******************************************************************/
int dniLite::GetNetfxSPLevel(const TCHAR *pszNetfxRegKeyName, const TCHAR *pszNetfxRegValueName)
{
	DWORD dwRegValue=0;

	if (RegistryGetValue(HKEY_LOCAL_MACHINE, pszNetfxRegKeyName, pszNetfxRegValueName, NULL, (LPBYTE)&dwRegValue, sizeof(DWORD)))
	{
		return (int)dwRegValue;
	}

	// We can only get here if the .NET Framework is not
	// installed or there was some kind of error retrieving
	// the data from the registry
	return -1;
}


/******************************************************************
Function Name:  GetProcessorArchitectureFlag
Description:    Determine the processor architecture of the
                system (x86, x64, ia64)
Inputs:         NONE
Results:        DWORD processor architecture flag
******************************************************************/
DWORD dniLite::GetProcessorArchitectureFlag()
{
	HMODULE hmodKernel32 = NULL;
	typedef void (WINAPI *PFnGetNativeSystemInfo) (LPSYSTEM_INFO);
	PFnGetNativeSystemInfo pfnGetNativeSystemInfo;

	SYSTEM_INFO sSystemInfo;
	memset(&sSystemInfo, 0, sizeof(sSystemInfo));

	bool bRetrievedSystemInfo = false;

	// Attempt to load kernel32.dll
	hmodKernel32 = LoadLibrary(_T("Kernel32.dll"));
	if (NULL != hmodKernel32)
	{
		// If the DLL loaded correctly, get the proc address for GetNativeSystemInfo
		pfnGetNativeSystemInfo = (PFnGetNativeSystemInfo) GetProcAddress(hmodKernel32, "GetNativeSystemInfo");
		if (NULL != pfnGetNativeSystemInfo)
		{
			// Call GetNativeSystemInfo if it exists
			(*pfnGetNativeSystemInfo)(&sSystemInfo);
			bRetrievedSystemInfo = true;
		}
		FreeLibrary(hmodKernel32);
	}

	if (!bRetrievedSystemInfo)
	{
		// Fallback to calling GetSystemInfo if the above failed
		GetSystemInfo(&sSystemInfo);
		bRetrievedSystemInfo = true;
	}

	if (bRetrievedSystemInfo)
	{
		switch (sSystemInfo.wProcessorArchitecture)
		{
			case PROCESSOR_ARCHITECTURE_INTEL:
				return RUNTIME_INFO_REQUEST_X86;
			case PROCESSOR_ARCHITECTURE_IA64:
				return RUNTIME_INFO_REQUEST_IA64;
			case PROCESSOR_ARCHITECTURE_AMD64:
				return RUNTIME_INFO_REQUEST_AMD64;
			default:
				return 0;
		}
	}

	return 0;
}


/******************************************************************
Function Name:	CheckNetfxBuildNumber
Description:	Retrieves the .NET Framework build number from
                the registry and validates that it is not a pre-release
                version number
Inputs:         NONE
Results:        true if the build number in the registry is greater
				than or equal to the passed in version; false otherwise
******************************************************************/
bool dniLite::CheckNetfxBuildNumber(const TCHAR *pszNetfxRegKeyName, const TCHAR *pszNetfxRegKeyValue, const int iRequestedVersionMajor, const int iRequestedVersionMinor, const int iRequestedVersionBuild, const int iRequestedVersionRevision)
{
	TCHAR szRegValue[MAX_PATH];
	TCHAR *pszToken = NULL;
	TCHAR *pszNextToken = NULL;
	int iVersionPartCounter = 0;
	int iRegistryVersionMajor = 0;
	int iRegistryVersionMinor = 0;
	int iRegistryVersionBuild = 0;
	int iRegistryVersionRevision = 0;
	bool bRegistryRetVal = false;

	// Attempt to retrieve the build number registry value
	bRegistryRetVal = RegistryGetValue(HKEY_LOCAL_MACHINE, pszNetfxRegKeyName, pszNetfxRegKeyValue, NULL, (LPBYTE)szRegValue, MAX_PATH);

	if (bRegistryRetVal)
	{
		// This registry value should be of the format
		// #.#.#####.##.  Try to parse the 4 parts of
		// the version here
		wxString retVal;
		retVal << szRegValue;
		wxStringTokenizer tkz(retVal, wxT("."));

		while(tkz.HasMoreTokens())
		{
			wxString token = tkz.GetNextToken();
			iVersionPartCounter++;

			switch (iVersionPartCounter)
			{
			case 1:
				// Convert the major version value to an integer
				iRegistryVersionMajor = wxAtoi(token);
				break;
			case 2:
				// Convert the minor version value to an integer
				iRegistryVersionMinor = wxAtoi(token);
				break;
			case 3:
				// Convert the build number value to an integer
				iRegistryVersionBuild = wxAtoi(token);
				break;
			case 4:
				// Convert the revision number value to an integer
				iRegistryVersionRevision = wxAtoi(token);
				break;
			default:
				break;

			}
		}
	}

	// Compare the version number retrieved from the registry with
	// the version number of the final release of the .NET Framework
	// that we are checking
	if (iRegistryVersionMajor > iRequestedVersionMajor)
	{
		return true;
	}
	else if (iRegistryVersionMajor == iRequestedVersionMajor)
	{
		if (iRegistryVersionMinor > iRequestedVersionMinor)
		{
			return true;
		}
		else if (iRegistryVersionMinor == iRequestedVersionMinor)
		{
			if (iRegistryVersionBuild > iRequestedVersionBuild)
			{
				return true;
			}
			else if (iRegistryVersionBuild == iRequestedVersionBuild)
			{
				if (iRegistryVersionRevision >= iRequestedVersionRevision)
				{
					return true;
				}
			}
		}
	}

	// If we get here, the version in the registry must be less than the
	// version of the final release of the .NET Framework we are checking,
	// so return false
	return false;
}


/******************************************************************
Function Name:  IsCurrentOSTabletMedCenter
Description:    Determine if the current OS is a Windows XP
                Tablet PC Edition or Windows XP Media Center
                Edition system
Inputs:         NONE
Results:        true if the OS is Tablet PC or Media Center
                false otherwise
******************************************************************/
bool dniLite::IsCurrentOSTabletMedCenter()
{
	// Use GetSystemMetrics to detect if we are on a Tablet PC or Media Center OS
	return ( (GetSystemMetrics(SM_TABLETPC) != 0) || (GetSystemMetrics(SM_MEDIACENTER) != 0) );
}


/******************************************************************
Function Name:  IsNetfx10Installed
Description:    Uses the detection method recommended at
                http://msdn.microsoft.com/library/ms994349.aspx
                to determine whether the .NET Framework 1.0 is
                installed on the machine
Inputs:         NONE
Results:        true if the .NET Framework 1.0 is installed
                false otherwise
******************************************************************/
bool dniLite::IsNetfx10Installed()
{
	TCHAR szRegValue[MAX_PATH];
	return (RegistryGetValue(HKEY_LOCAL_MACHINE, g_szNetfx10RegKeyName, g_szNetfx10RegKeyValue, NULL, (LPBYTE)szRegValue, MAX_PATH));
}


/******************************************************************
Function Name:  IsNetfx11Installed
Description:    Uses the detection method recommended at
                http://msdn.microsoft.com/library/ms994339.aspx
                to determine whether the .NET Framework 1.1 is
                installed on the machine
Inputs:         NONE
Results:        true if the .NET Framework 1.1 is installed
                false otherwise
******************************************************************/
bool dniLite::IsNetfx11Installed()
{
	bool bRetValue = false;
	DWORD dwRegValue=0;

	if (RegistryGetValue(HKEY_LOCAL_MACHINE, g_szNetfx11RegKeyName, g_szNetfxStandardRegValueName, NULL, (LPBYTE)&dwRegValue, sizeof(DWORD)))
	{
		if (1 == dwRegValue)
			bRetValue = true;
	}

	return bRetValue;
}


/******************************************************************
Function Name:	IsNetfx20Installed
Description:	Uses the detection method recommended at
                http://msdn2.microsoft.com/library/aa480243.aspx
                to determine whether the .NET Framework 2.0 is
                installed on the machine
Inputs:         NONE
Results:        true if the .NET Framework 2.0 is installed
                false otherwise
******************************************************************/
bool dniLite::IsNetfx20Installed()
{
	bool bRetValue = false;
	DWORD dwRegValue=0;

	if (RegistryGetValue(HKEY_LOCAL_MACHINE, g_szNetfx20RegKeyName, g_szNetfxStandardRegValueName, NULL, (LPBYTE)&dwRegValue, sizeof(DWORD)))
	{
		if (1 == dwRegValue)
			bRetValue = true;
	}

	return bRetValue;
}


/******************************************************************
Function Name:	IsNetfx30Installed
Description:	Uses the detection method recommended at
                http://msdn.microsoft.com/library/aa964979.aspx
                to determine whether the .NET Framework 3.0 is
                installed on the machine
Inputs:	        NONE
Results:        true if the .NET Framework 3.0 is installed
                false otherwise
******************************************************************/
bool dniLite::IsNetfx30Installed()
{
	bool bRetValue = false;
	DWORD dwRegValue=0;

	// Check that the InstallSuccess registry value exists and equals 1
	if (RegistryGetValue(HKEY_LOCAL_MACHINE, g_szNetfx30RegKeyName, g_szNetfx30RegValueName, NULL, (LPBYTE)&dwRegValue, sizeof(DWORD)))
	{
		if (1 == dwRegValue)
			bRetValue = true;
	}

	// A system with a pre-release version of the .NET Framework 3.0 can
	// have the InstallSuccess value.  As an added verification, check the
	// version number listed in the registry
	return (bRetValue && CheckNetfxBuildNumber(g_szNetfx30RegKeyName, g_szNetfxStandardVersionRegValueName, g_iNetfx30VersionMajor, g_iNetfx30VersionMinor, g_iNetfx30VersionBuild, g_iNetfx30VersionRevision));
}


/******************************************************************
Function Name:	IsNetfx35Installed
Description:	Uses the detection method recommended at
                http://msdn.microsoft.com/library/cc160716.aspx
                to determine whether the .NET Framework 3.5 is
                installed on the machine
Inputs:	        NONE
Results:        true if the .NET Framework 3.5 is installed
                false otherwise
******************************************************************/
bool dniLite::IsNetfx35Installed()
{
	bool bRetValue = false;
	DWORD dwRegValue=0;

	// Check that the Install registry value exists and equals 1
	if (RegistryGetValue(HKEY_LOCAL_MACHINE, g_szNetfx35RegKeyName, g_szNetfxStandardRegValueName, NULL, (LPBYTE)&dwRegValue, sizeof(DWORD)))
	{
		if (1 == dwRegValue)
			bRetValue = true;
	}

	// A system with a pre-release version of the .NET Framework 3.5 can
	// have the Install value.  As an added verification, check the
	// version number listed in the registry
	return (bRetValue && CheckNetfxBuildNumber(g_szNetfx35RegKeyName, g_szNetfxStandardVersionRegValueName, g_iNetfx35VersionMajor, g_iNetfx35VersionMinor, g_iNetfx35VersionBuild, g_iNetfx35VersionRevision));
}


/******************************************************************
Function Name:	IsNetfx40ClientInstalled
Description:	Uses the detection method recommended at
                http://msdn.microsoft.com/library/ee942965(v=VS.100).aspx
                to determine whether the .NET Framework 4 Client is
                installed on the machine
Inputs:         NONE
Results:        true if the .NET Framework 4 Client is installed
                false otherwise
******************************************************************/
bool dniLite::IsNetfx40ClientInstalled()
{
	bool bRetValue = false;
	DWORD dwRegValue=0;

	if (RegistryGetValue(HKEY_LOCAL_MACHINE, g_szNetfx40ClientRegKeyName, g_szNetfxStandardRegValueName, NULL, (LPBYTE)&dwRegValue, sizeof(DWORD)))
	{
		if (1 == dwRegValue)
			bRetValue = true;
	}

	// A system with a pre-release version of the .NET Framework 4 can
	// have the Install value.  As an added verification, check the
	// version number listed in the registry
	return (bRetValue && CheckNetfxBuildNumber(g_szNetfx40ClientRegKeyName, g_szNetfxStandardVersionRegValueName, g_iNetfx40VersionMajor, g_iNetfx40VersionMinor, g_iNetfx40VersionBuild, g_iNetfx40VersionRevision));
}


/******************************************************************
Function Name:	IsNetfx40FullInstalled
Description:	Uses the detection method recommended at
                http://msdn.microsoft.com/library/ee942965(v=VS.100).aspx
                to determine whether the .NET Framework 4 Full is
                installed on the machine
Inputs:         NONE
Results:        true if the .NET Framework 4 Full is installed
                false otherwise
******************************************************************/
bool dniLite::IsNetfx40FullInstalled()
{
	bool bRetValue = false;
	DWORD dwRegValue=0;

	if (RegistryGetValue(HKEY_LOCAL_MACHINE, g_szNetfx40FullRegKeyName, g_szNetfxStandardRegValueName, NULL, (LPBYTE)&dwRegValue, sizeof(DWORD)))
	{
		if (1 == dwRegValue)
			bRetValue = true;
	}

	// A system with a pre-release version of the .NET Framework 4 can
	// have the Install value.  As an added verification, check the
	// version number listed in the registry
	return (bRetValue && CheckNetfxBuildNumber(g_szNetfx40FullRegKeyName, g_szNetfxStandardVersionRegValueName, g_iNetfx40VersionMajor, g_iNetfx40VersionMinor, g_iNetfx40VersionBuild, g_iNetfx40VersionRevision));
}


/******************************************************************
Function Name:	IsNetfx45Installed
Description:	Uses the detection method recommended at
                http://msdn.microsoft.com/en-us/library/ee942965.aspx
                to determine whether the .NET Framework 4.5 is
                installed on the machine
Inputs:         NONE
Results:        true if the .NET Framework 4.5 is installed
                false otherwise
******************************************************************/
bool dniLite::IsNetfx45Installed()
{
	bool bRetValue = false;
	DWORD dwRegValue=0;
	wxString Netfx45TargetKey;
//	wxString output;

if(IsNetfx40FullInstalled())
{
	Netfx45TargetKey = g_szNetfx45FullRegKeyName;
//	output << ".NET 4.0 Full Installed";
}
else
{
	Netfx45TargetKey = g_szNetfx45ClientRegKeyName;
//	output << ".NET 4.0 Client Installed";
}

	if (RegistryGetValue(HKEY_LOCAL_MACHINE, Netfx45TargetKey, g_szNetfx45RegValueName, NULL, (LPBYTE)&dwRegValue, sizeof(DWORD)))
	{
		if (378389 <= dwRegValue)
			bRetValue = true;
	}

/*	output << "\nKey: " << Netfx45TargetKey;
	output << "\nValue: " << g_szNetfxStandardRegValueName;
	output << "\nInstalled: " << (bRetValue ? "true" : "false");
	output << "\n";
	output << "\nKey: " << Netfx45TargetKey;
	output << "\nValue: " << g_szNetfxStandardVersionRegValueName;
	output << "\nMajor: " << g_iNetfx45VersionMajor;
	output << "\nMinor: " << g_iNetfx45VersionMinor;
	output << "\nBuild: " << g_iNetfx45VersionBuild;
	output << "\nRevision: " << g_iNetfx45VersionRevision;
//	output << "\nFound: " << RegistryGetValue(HKEY_LOCAL_MACHINE, Netfx45TargetKey, g_szNetfxStandardVersionRegValueName, NULL, (LPBYTE)szRegValue, MAX_PATH);
	output << "\nFound: " << CheckNetfxBuildNumber(Netfx45TargetKey, g_szNetfxStandardVersionRegValueName, g_iNetfx45VersionMajor, g_iNetfx45VersionMinor, g_iNetfx45VersionBuild, g_iNetfx45VersionRevision);


	wxMessageBox(output);
*/	// A system with a pre-release version of the .NET Framework 4.5 can
	// have the Install value.  As an added verification, check the
	// version number listed in the registry

return bRetValue;
	// do not need to check for version number here, that check has already been performed above
//	return (bRetValue && CheckNetfxBuildNumber(Netfx45TargetKey, g_szNetfxStandardVersionRegValueName, g_iNetfx45VersionMajor, g_iNetfx45VersionMinor, g_iNetfx45VersionBuild, g_iNetfx45VersionRevision));
}


/******************************************************************
Function Name:  RegistryGetValue
Description:    Get the value of a reg key
Inputs:         HKEY hk - The hk of the key to retrieve
                TCHAR *pszKey - Name of the key to retrieve
                TCHAR *pszValue - The value that will be retrieved
                DWORD dwType - The type of the value that will be retrieved
                LPBYTE data - A buffer to save the retrieved data
                DWORD dwSize - The size of the data retrieved
Results:        true if successful, false otherwise
******************************************************************/
bool dniLite::RegistryGetValue(HKEY hk, const TCHAR * pszKey, const TCHAR * pszValue, DWORD dwType, LPBYTE data, DWORD dwSize)
{
	HKEY hkOpened;

	// Try to open the key
	if (RegOpenKeyEx(hk, pszKey, 0, KEY_READ, &hkOpened) != ERROR_SUCCESS)
	{
		return false;
	}

	// If the key was opened, try to retrieve the value
	if (RegQueryValueEx(hkOpened, pszValue, 0, &dwType, (LPBYTE)data, &dwSize) != ERROR_SUCCESS)
	{
		RegCloseKey(hkOpened);
		return false;
	}

	// Clean up
	RegCloseKey(hkOpened);

	return true;
}
