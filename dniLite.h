//---------------------------------------------------------------------------
//
// Name:        dniLite.h
// Author:      Firedancer Software
// Created:     22/06/2013 1:48:36 PM
// Description:
//
//---------------------------------------------------------------------------

#ifndef __dniLite_h__
#define __dniLite_h__

#ifdef __BORLANDC__
	#pragma hdrstop
#endif

#ifndef WX_PRECOMP
	#include <wx/wx.h>
#else
	#include <wx/wxprec.h>
#endif

#include <wx/cmdline.h>
#include <windows.h>

class dniLite : public wxAppConsole
{
	public:
		bool OnInit();
		int OnExit();
		wxCmdLineParser parser;
		bool bCheckVersion;
		bool bCheckGreaterThan;
		bool bCheckLessThan;
		wxString version;
		bool bCheckServicePack;
		long lServicePack;
		bool bExecuteIfFound;
		wxString executeIfFoundPath;
		bool bExecuteIfNotFound;
		wxString executeIfNotFoundPath;

		bool bStdoutMessageFound;
		wxString stdoutMessageFound;
		bool bStdoutMessageNotFound;
		wxString stdoutMessageNotFound;

		bool bDialogMessageFound;
		wxString dialogMessageFound;
		bool bDialogMessageNotFound;
		wxString dialogMessageNotFound;

	private:
		bool CheckNetfxBuildNumber(const TCHAR*, const TCHAR*, const int, const int, const int, const int);
		//bool CheckNetfxVersionUsingMscoree(const TCHAR*);
		int GetNetfx10SPLevel();
		int GetNetfxSPLevel(const TCHAR*, const TCHAR*);
		DWORD GetProcessorArchitectureFlag();
		bool IsCurrentOSTabletMedCenter();
		bool IsNetfx10Installed();
		bool IsNetfx11Installed();
		bool IsNetfx20Installed();
		bool IsNetfx30Installed();
		bool IsNetfx35Installed();
		bool IsNetfx40ClientInstalled();
		bool IsNetfx40FullInstalled();
		bool IsNetfx45Installed();
		bool RegistryGetValue(HKEY, const TCHAR*, const TCHAR*, DWORD, LPBYTE, DWORD);

typedef enum {

    RUNTIME_INFO_UPGRADE_VERSION         = 0x01,
    RUNTIME_INFO_REQUEST_IA64            = 0x02,
    RUNTIME_INFO_REQUEST_AMD64           = 0x04,
    RUNTIME_INFO_REQUEST_X86             = 0x08,
    RUNTIME_INFO_DONT_RETURN_DIRECTORY   = 0x10,
    RUNTIME_INFO_DONT_RETURN_VERSION     = 0x20,
    RUNTIME_INFO_DONT_SHOW_ERROR_DIALOG  = 0x40

} RUNTIME_INFO_FLAGS;

};

static const wxCmdLineEntryDesc g_cmdLineDesc [] =
{
	{ wxCMD_LINE_OPTION, "fv", "framework-version", ".NET Framework version to check (1.0, 1.1, 2.0, 3.0, 3.5, 4.0, 4.0client, 4.0full or 4.5)",
		wxCMD_LINE_VAL_STRING, wxCMD_LINE_NEEDS_SEPARATOR | wxCMD_LINE_OPTION_MANDATORY },
	{ wxCMD_LINE_SWITCH, "gt", "greater-than", "Only return true for versions greater than -fv (optional)",
		wxCMD_LINE_VAL_STRING, wxCMD_LINE_NEEDS_SEPARATOR },
	{ wxCMD_LINE_SWITCH, "lt", "less-than", "Only return true for versions less than -fv (optional)",
		wxCMD_LINE_VAL_STRING, wxCMD_LINE_NEEDS_SEPARATOR },
	{ wxCMD_LINE_OPTION, "sp", "service-pack", "Check for specific service pack (optional: use 0 to test for no service pack)",
		wxCMD_LINE_VAL_NUMBER, wxCMD_LINE_NEEDS_SEPARATOR },
	{ wxCMD_LINE_OPTION, "et", "execute-true", "File to run if test returns true (optional)",
		wxCMD_LINE_VAL_STRING, wxCMD_LINE_NEEDS_SEPARATOR },
	{ wxCMD_LINE_OPTION, "ef", "execute-false", "File to run if test returns false (optional)",
		wxCMD_LINE_VAL_STRING, wxCMD_LINE_NEEDS_SEPARATOR },
	{ wxCMD_LINE_OPTION, "smt", "stdout-message-true", "Message output via stdout if test returns true (optional | default=1)",
		wxCMD_LINE_VAL_STRING, wxCMD_LINE_NEEDS_SEPARATOR },
	{ wxCMD_LINE_OPTION, "smf", "stdout-message-false", "Message output via stdout if test returns false (optional | default=0)",
		wxCMD_LINE_VAL_STRING, wxCMD_LINE_NEEDS_SEPARATOR },
	{ wxCMD_LINE_OPTION, "dmt", "dialog-message-true", "Message output via dialog if test returns true (optional)",
		wxCMD_LINE_VAL_STRING, wxCMD_LINE_NEEDS_SEPARATOR },
	{ wxCMD_LINE_OPTION, "dmf", "dialog-message-false", "Message output via dialog if test returns false (optional)",
		wxCMD_LINE_VAL_STRING, wxCMD_LINE_NEEDS_SEPARATOR },
	{ wxCMD_LINE_SWITCH, "h", "help", "Displays help on the command line parameters",
		wxCMD_LINE_VAL_NONE, wxCMD_LINE_OPTION_HELP },
	{ wxCMD_LINE_NONE }
};

DECLARE_APP(dniLite)
#endif
